#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_pdx206.mk

COMMON_LUNCH_CHOICES := \
    omni_pdx206-user \
    omni_pdx206-userdebug \
    omni_pdx206-eng
