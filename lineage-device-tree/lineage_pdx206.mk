#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from pdx206 device
$(call inherit-product, device/sony/pdx206/device.mk)

PRODUCT_DEVICE := pdx206
PRODUCT_NAME := lineage_pdx206
PRODUCT_BRAND := Sony
PRODUCT_MODEL := Pdx206
PRODUCT_MANUFACTURER := sony

PRODUCT_GMS_CLIENTID_BASE := android-sonymobile-rvo3

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="pdx206-user 11 RKQ1.211001.001 1 release-keys"

BUILD_FINGERPRINT := Sony/pdx206/pdx206:11/RKQ1.211001.001/1:user/release-keys
